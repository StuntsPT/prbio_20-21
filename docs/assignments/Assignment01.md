# Assignment 01

### Deadline:

December 10th 2020


### Format:

Work in groups of 4


### Delivery:

PDF via E-mail


## Introduction

During the last few classes you have learned to create new Virtual Machines (VMs), and to install a GNU/Linux based Operating System (OS).
Furthermore you have learned how to navigate a \*NIX shell, and use some of it's programs.
You will have to use these new skills combined with some **research** to complete your assignment.


## Objectives

* Create a new VM, and install a new GNU/Linux based OS on it that is **not** *Ubuntu* or one of it's close derivatives (*kubuntu*, *lubuntu*, *xubuntu*, etc..).
* Find and provide information about the kernel your OS is using (Take a screenshot).
* Find the **configuration file** containing the list of users registered on the system, filter the line containing your current user and report it.
* Bonus points if you can describe each section separated by ":".
* Provide a screenshot of the desktop.
* Install the program `htop`, run it and provide a screenshot.
* Export your VM as an appliance and get the SHA256SUM value of the generated `.ova` file. [Here](https://superuser.com/a/898377) is one way to generate this hash on Windows. But there are alternative ways.
  - Keep this file until your work is graded. It may be requested.
* Bonus points if you provide a **brief** explanation on each of these two concepts (a SHA256SUM and a `.ova` file).


## Methodology

* You can use any text processing software to write your report. But it has to be sent as a finished document, Eg. PDF or HTML formats.
* Your report should have *at least* the following sections:
    * Introduction - where you describe the GNU/Linux distro you have chosen (provide a logo), and a short sentence on its History. This section **must** state which package manger is used by the chosen OS, and the respective package format.
    * Results - where you describe what is requested in the objectives.
    * Bibliography - this should go without saying, but still - reference were you got your information from.
* The report can either be provided in English or Portuguese. It's your choice.
* Don't forget to include a title and a cover page!
* Do not forget to include the SHA256SUM hash for the `.ova` file in the report.
* Keep your `.ova` file for a while since some will be randomly requested.
* Every figure in the report **has to be referenced** in the text!
* Let me repeat that: Every figure in the report **has to be referenced** in the text!
* Make sure you proofread your work. You only get one shot.
